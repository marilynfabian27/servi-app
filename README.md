#### 프로젝트 초기화
저장소 복제 후 최초 한번 프로젝트를 초기화 한다.
```bash
npm install express
```

# 사용자 등록 및 로그인 처리
> 새로운 사용자 정보를 등록하거나 수정하기 위해서는 사용자로 부터 입력 받은 값에 대하여 유효성 검사를 하여야 한다. Joi 모듈을 사용하면 이와 같은 기능을 기정의된 스크립트를 이용하여 간단하게 구현할 수 있다. 또한 비밀번호를 저장할 경우 암호화 하여 저장하도록 한다.   
사용자 등록 처리가 완료되면 로그인 처리를 위해 사용자 정보(아이디, 패스워드)와 등록된 정보를 비교하여 로그인 처리를 한다.

- joi validation 정의
- joi valdate 적용
- 사용자 중복 검사
- 비밀번호 암호화 적용
- 로그인 처리 

### joi validation 정의
사용자로 부터 입력 받은 정보에 대해서 유효성을 검사하는 기능은 joi모듈을 이용하면 쉽게 구현할 수 있다.
먼저, 모듈을 추가 한다.
```bash
npm install joi --save
```


#### 1) validation 파일 생성  
프로젝트 root 디렉토리에 validation 폴더를 생성하고 validation.js 파일을 추가한다.
```
~/validation/validation.js
```  

validation.js 파일에 joi 모듈을 추가한다.
```javascript   
const Joi = require('joi');
```


#### 2) 사용자 등록 validation 정의
제일먼저 validation을 위한 스키마를 생성한다. 아래 예제에서와 같이 사용법은 직관적이라 이해해는데 어려움이 없다. email()의 경우 이메일 양식을 체크하기 위해 제공해 주는 함수이다.  
  input값으로 data 객체를 받아서 Joi.validate() 함수에 정의한 스키마와 같이 인자를 넘겨주면 유효성 검사후 결과 값을 반환하게 된다.
```javascript   
const registerValidation = (data) => {

    const schema = {
        name: Joi.string().min(6).required(),
        email: Joi.string().min(6).required().email(),
        password: Joi.string().min(6).required()
    };
    return Joi.validate(data,schema);

};
```
정의한 모듈을 exprots 한다. 복수개의 유효성 함수를 정의 하였으면 다음과 같이추가적으로 exports해 준다.

```javascript   
module.exports.registerValidation = registerValidation;
module.exports.loginValidation = loginValidation;
```   
validation.js에 복수개의 유효성 검사 함수를 추가할 수 있다. 개수가 많아 질 경우 파일을 분리하여 저장한다.





### joi valdate 적용
api 모듈에 정의한 validation 모듈을 적용해 보자.

#### 1) validation.js import
validation.js에 선어한 함수를 사용하기 위해 먼저 Import를 한다. auth.js 파일을 열어 아래 코드를 추가한다. {}를 이용하여 복수개를 한번에 지정할 수 있다.   
```javascript   
const { registerValidation, loginValidation } = require('../validation/validation-user'); // joi를 이용한 validation  function 
```   


#### 2) auth.js 수정하기 
사용자 등록 api 에 사용자 입력값을 체킹하는 로직을 추가하다. registerValidation 함수에 req.body 값을 인자로 넣어주면 된다. 
유효성 처리 결과를 위한 코드를 위해 결과값을  error 객체로 응답을 받아 처리 로직을 추가 한다.
에러가 발생하면 발생된 첫번째 에러 결과물을 화면에 리턴해 준다. 
```javascript   
router.post('/register', async (req, res) => {
    const { error } = registerValidation(req.body);
    if(error)  return res.status(400).send(error.details[0].message);
}
```  
만약 화면에서 별도로 처리할 경우
res.status(400).send(error) 로 변경하고 화면에서 입력폼과 바인딩하여 에러 로그를 출력해 줄 수 있다. 
```javascript   
    if(error)  return res.status(400).send(error);
```  


### 사용자 중복 검사
 사용자를 등록하기 앞서 등록한 사용자가 있는지 확인할 필요가 있다. User.findeOne() 메소드를 이용하면 디비에 매칭되는 정보를 반환한다. 인자값으로 email을 넘겨주면 매칭되는 값을 찾아 객체를 찾게 된다. 등록된 사용자가 있으면 등록된 사용자라는 메시지와 함께 400코드를 반환한다.   
 등록된 사용자가 없는 경우 디비에 저장 처리를 한다.
```javascript   
    const emailExist = await User.findOne({email : req.body.email});
    if(emailExist) res.status(400).send('Email already exists');
```  







### 비밀번호 암호화하여 저장하기

```bash
npm install bcryptjs --save
```

암호화 모듈을 사용하기 위해 모듈을 import 한다. 
```javascript 
const bcrypt = require('bcryptjs'); // 암호화 
```  

암호화를 하기전에 salt값을 넣어 암호화의 강도를 높여 해커로부터 안전하게 비밀번호를 보호한다,
[salt를 사용하는 이유](https://jeong-pro.tistory.com/92)
```javascript 
const salt = await bcrypt.genSalt(10);
const hashedPassword = await bcrypt.hash(req.body.password , salt);
```  
디비에 저장하는 값을 암호화된 값으로 대체한다. 이전에는 req.body.password값 즉, 평문을 그대로 저장하여 비밀번호 값이 노출되었는데 이를 암호화된 값으로 치환하여 저장하도록 코드를 수정해 준다.
```javascript 
const user = new User({ // 몽고 디비에 저장하기 위해 스키마에 값을 바인딩 한다.
    name: req.body.name,
    email: req.body.email,
    password: hashedPassword // 암호화된 값으로 변경 
});
``` 
- 이전에 저장된 값은 평문이지만 암호화 로직을 추가한 후 저장된 값은 해싱된 값으로 들어가 있음을 확인할 수 있다.
![password](./images/pwd_01.png)

### 로그인 하기
로그인처리를 하기 위해서는 사용자로 부터 아이디와 비밀번호를 입력 받아야 한다. 입력된 값은 기본적으로 동일한 validation 정책을 가지고 간다. 최근에는 이메일을 아이디로 사용하는 경우가 많으므로 본 예제에서는 이메일과 비밀번호를 입력받아 로그인 처리를 진행하는 로직을 구현하도록 한다.   


#### 1) 로그인 API 추가하기
auth.js에 로그인 api를 추가한다. 
```javascript 
router.post('/login', async (req, res) => {

});
```  


#### 2) 유효성 검사
유효성 검사를 위한 함수를 validation.js에 추가로 정의하고 이를 사용한다.
사용자 등록을 위해 사용한 코드와 동일하게 함수명만 새로 정의한 loginValidation로 변경해 준다.
에러가 발생하면 화면에 에러 메시지를 전달 하도록 한다.   
```javascript 
const { error } = loginValidation(req.body);
if(error)  return res.status(400).send(error.details[0].message);
```  
- 유효성 검사 에러 메시지 출력
![password](./images/login_01.png)   


#### 3) 기 등록된 정보 확인
사용자로 부터 입력 받은 정보로 디비에 정보가 존재하는지 조회한다.
디비 값이 없는 경우 400 코드와 함께 에러 메시지를 리턴한다.
```javascript 
// 기 등록된 계정 정보가 있는지 확인
const userExist = await User.findOne({email : req.body.email});
if(!userExist) res.status(400).send('Email is not found');

```  

#### 4) 패스워드 비교 
암호화된 정보와 사용자로 부터 입력 받은 값을 비교하여 패스워드가 일치하는지 확인한다. 디비로 부터 얻은 암호화된 패스워드와 사용자로 부터 입력 받은 평문 패스워드를 bycrpt.compare()함수를 이용하여 비교한다. 비교 결과값이 null 이면 400코드와 함께 에러 메시지를 리턴한다.
```javascript 
const comparePass = await bcrypt.compare(req.body.password, userExist.password);
if(!comparePass) return res.status(400).send('Invalid password');
```  
- 잘못된 경우 에러 메시지 출력
![password](./images/login_02.png)   


#### 5) 로그인 처리
로그인이 완료되면 로그인 완료 처리 메시지를 화면에 보낸다.
이후 로그인 완료후 메인 화면으로 리다이렉션 처리로 변경할 수 있다.
```javascript 
res.send('Success Login!'); 
```  
- 성공시 로그인 처리
![password](./images/login_03.png)